/*
 * Created by instancetype on 5/29/14.
 */

function List() {
    // initial conditions of the list
    this.dataStore = [];
    this.listSize = 0;
    this.position = 0;

    // altering contents
    this.append = append;
    this.remove = remove;
    this.insertAfter = insertAfter;
    this.clear = clear;

    // traversing the list
    this.front = front;
    this.end = end;
    this.previous = previous;
    this.next = next;
    this.moveToPosition = moveToPosition;

    // getting an element
    this.getElement = getElement;

    // inspecting the list
    this.currentPosition = currentPosition;
    this.length = length;
    this.find = find;
    this.contains = contains;
    this.toString = toString;
}

function append(element) {
    this.dataStore[this.listSize++] = element;
}

function find(element) {
    var i,
        length = this.dataStore.length;

    for (i = 0; i < length; ++i) {
        if (this.dataStore[i] === element) {
            return i;
        }
    }
    return -1;
}

function remove(element) {
    var idxOfElement = this.find(element);
    if (idxOfElement > -1) {
        this.dataStore.splice(idxOfElement, 1);
        --this.listSize;
        return true;
    }
    return false;
}

function insertAfter(element, target) {
    var insertPosition = this.find(target);
    if (insertPosition > -1) {
        this.dataStore.splice(insertPosition+1, 0, element);
        ++this.listSize;
        return true;
    }
    return false;
}

function clear() {
    delete this.dataStore;
    this.dataStore = [];
    this.listSize = 0;
    this.position = 0;
}

function front() {
    this.position = 0;
}

function end() {
    this.position = this.listSize - 1;
}

function previous() {
    if (this.position > 0) {
        --this.position;
    }
}

function next() {
    if (this.position < this.listSize-1) {
        ++this.position;
    }
}

function moveToPosition(pos) {
    if (pos >= 0 && pos < this.listSize) {
        this.position = pos;
    }
}

function getElement() {
    return this.dataStore[this.position];
}

function currentPosition() {
    return this.position;
}

function length() {
    return this.listSize;
}

function contains(element) {
    var i,
        length = this.dataStore.length;

    for (i = 0; i < length; ++i) {
        if (this.dataStore[i] === element) {
            return true;
        }
    }
    return false;
}

function toString() {
    return this.dataStore;
}

module.exports = List;