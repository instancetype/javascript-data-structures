/**
 * Created by instancetype on 6/6/14.
 */
var Stack = require('./stack');

function isPalindrome(word) {
    var s = new Stack();
    for (var i = 0; i < word.length; ++i) {
        s.push(word[i]);
    }

    var reversed = '';
    while (s.length() > 0) {
        reversed += s.pop();
    }

    return word.toLowerCase() === reversed.toLowerCase();
}

function logPalindromeStatus(word) {
    if (isPalindrome(word)) console.log(word + ' is a palindrome.');
    else console.log(word + ' is not a palindrome.');
}

logPalindromeStatus('D');
logPalindromeStatus('Da');
logPalindromeStatus('Dad');
logPalindromeStatus('Whatever');
logPalindromeStatus('Racecar');