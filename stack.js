/*
 * Created by instancetype on 5/29/14.
 */
function Stack() {
    // initial conditions of the stack
    this.dataStore = [];
    this.top = 0;

    // primary operations
    this.push = push;
    this.pop = pop;
    this.peek = peek;

    // auxiliary operations
    this.clear = clear;
    this.length = length;
    this.isEmpty = isEmpty;
}

function push(item) {
    this.dataStore[this.top++] = item;
}

function pop() {
    return this.dataStore[--this.top];
}

function peek() {
    return this.dataStore[top-1];
}

function length() {
    return this.top;
}

function isEmpty() {
    return this.top === 0;
}

function clear() {
    this.top = 0;
}

module.exports = Stack;