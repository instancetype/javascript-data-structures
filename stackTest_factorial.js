/**
 * Created by instancetype on 6/6/14.
 */
var Stack = require('./stack');

function factorial(n) {
    var s = new Stack();
    while (n > 1) {
        s.push(n--);
    }

    var product = 1;
    while (s.length() > 0) {
        product *= s.pop();
    }
    return product;
}

function logFactorial(n) {
    console.log(n + '! is ' + factorial(n));
}

logFactorial(3);
logFactorial(5);
logFactorial(8);
